#!/usr/bin/env bash
/usr/local/bin/python manage.py migrate
/usr/local/bin/python manage.py collectstatic --noinput
/usr/local/bin/gunicorn --timeout 240 backend.wsgi:application -w 2 -b :8000
/bin/bash
