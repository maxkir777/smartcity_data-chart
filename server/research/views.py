from django.shortcuts import render
from rest_framework import generics
from .models import Research
from .serializers import ResearchSerializer


class ResearchListAPIView(generics.ListAPIView):
    queryset = Research.objects.all()
    serializer_class = ResearchSerializer
