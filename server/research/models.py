from django.db import models


class AIModel(models.Model):
    title = models.CharField('Название', max_length=100)
    file = models.FileField('Файл', upload_to='ai_model_files')
    image = models.ImageField('Изображения', upload_to='ai_model_images')
    accuracy = models.DecimalField(max_digits=5, decimal_places=2, verbose_name='Точность')

    class Meta:
        verbose_name = 'Модель ИИ'
        verbose_name_plural = 'Модели ИИ'
        ordering = ['accuracy']

    def __str__(self):
        return self.title


class Research(models.Model):
    title = models.CharField('Название', max_length=100)
    data = models.FileField(upload_to='data_learning', verbose_name='Данные для обучения')
    ai_models = models.ManyToManyField(AIModel, verbose_name='Модели')
    image = models.ImageField('Изображение', upload_to='research_images')
    description = models.TextField('Описание')

    class Meta:
        verbose_name = 'Исследование'
        verbose_name_plural = 'Исследования'

    def __str__(self):
        return self.title
