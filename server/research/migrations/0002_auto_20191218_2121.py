# Generated by Django 2.2.8 on 2019-12-18 21:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('research', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='aimodel',
            name='file',
            field=models.FileField(upload_to='ai_model_files', verbose_name='Файл'),
        ),
    ]
