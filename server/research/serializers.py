from rest_framework import serializers
from .models import Research, AIModel


class AIModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = AIModel
        fields = ('id', 'title', 'image', 'accuracy')


class ResearchSerializer(serializers.ModelSerializer):
    ai_models = AIModelSerializer(many=True, read_only=True)

    class Meta:
        model = Research
        fields = ('id', 'title', 'image', 'description', 'ai_models')
