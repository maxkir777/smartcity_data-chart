from django.contrib import admin
from .models import AIModel, Research


@admin.register(AIModel)
class AIModelAdmin(admin.ModelAdmin):
    pass


@admin.register(Research)
class ResearchAdmin(admin.ModelAdmin):
    pass
